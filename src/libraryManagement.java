import org.apache.log4j.*;
import org.nocrala.tools.texttablefmt.*;
import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

public class libraryManagement {
//    declaration array object and array for store data
    final  int indexBook =3;
    Book[] books = new Book[indexBook];
    int id=0,publishedYear,idAuto=0;
    String title,author,status;
    String ID[] = new String[indexBook];
    String Title[] = new String[indexBook];
    String Author[] = new String[indexBook];
    String Status[] = new String[indexBook];
    String py[] = new String[indexBook];



    //    this Method for input only numbers
    public  int validationNumber(String st){
        Scanner sc = new Scanner(System.in);
        int num=0;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            if (str.matches("[0-9][0-9]*")){
                num = Integer.parseInt(str);
                break;
            }
            else
                System.out.println("\nPLEASE INPUT NUMBERS\n");
        }
        return num;
    }

    //    this Method for input only String
    public  String validationString(String st){
        Scanner sc = new Scanner(System.in);
        String values;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            //^(.*\s+.*)+$
            if (str.matches("[a-zA-Z]+\\.?")){
                values =str;
                break;
            }
            else
                System.out.println("\nPLEASE INPUT STRING WITH NO WHITESPACE\n");
        }
        return values;
    }
    // this method for press enter to continue...
    public  void promptEnterKey(){
        System.out.println("Press \"ENTER\" to continue...");
        try {
            int read = System.in.read(new byte[2]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //    this Method for Set up library's name and Address
    public  String setup_library(){
        String libraryName,libraryAddress;
        Date ct= new Date();
        System.out.println("========= SET UP LIBRARY =========\n");
        libraryName = validationString("=> Enter Library's Name : ");
        libraryAddress = validationString("=> Enter Library's Address : ");
        System.out.println('"'+libraryName+'"'+
                            "library is already created in "+
                            '"'+libraryAddress+'"' +
                            "address successfully on"+ct);
        return libraryName+"|"+libraryAddress;
    }
    //    this Method for Add new book
    public void addBook(int idAuto){
        if (idAuto<indexBook){
            id= idAuto+1;// auto id
            System.out.println("=> Book ID              : "+id);
            title = validationString("=> Enter Book's Name    : ");
            author = validationString("=> Enter Book Author    : ");
            publishedYear = validationNumber("=> Enter Published Year : ");
            status ="Available";

            //set data into array object by using constructor
            books[idAuto]= new Book(id,title,author,publishedYear,status);

            //get data from array object into array for show in table
            ID[idAuto] = String.valueOf(books[idAuto].getId());
            Title[idAuto] = books[idAuto].getTitle();
            Author[idAuto] = books[idAuto].getAuthor();
            Status[idAuto] = books[idAuto].getStatus();
            py[idAuto] = String.valueOf(books[idAuto].getPublishedYear());
            System.out.println(" Book is added successfully");
            idAuto++;
        }else {
            System.out.println("Fully! You can't add Books more!");
        }

        promptEnterKey();
    }
    /* this method for show book with condition
        1.condition= "Available" show only Available books
        2.condition ="All" show All books
    */
    public void showBook(String condition){
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.ALL,false,"");
        int count=0;
        if (id > 0) {

            Logger.getRootLogger().setLevel(Level.OFF);

            t.addCell("ID", cs);
            t.addCell("TITLE", cs);
            t.addCell("AUTHOR", cs);
            t.addCell("PUBLISHED DATE", cs);
            t.addCell("STATUS", cs);
            for (int i=0;i<id;i++){
                //show only Available Books
                if (condition=="Available"){
                    if(Status[i]==condition){
                        count++;
                        t.addCell(ID[i],cs);
                        t.addCell(Title[i],cs);
                        t.addCell(Author[i],cs);
                        t.addCell(py[i],cs);
                        t.addCell(Status[i],cs);
                    }
                }
                //show only All Books
                else{
                    count++;
                    t.addCell(ID[i],cs);
                    t.addCell(Title[i],cs);
                    t.addCell(Author[i],cs);
                    t.addCell(py[i],cs);
                    t.addCell(Status[i],cs);
                }

            }
            if (count>0){
                System.out.println(t.render());
            }else
                System.out.println(" No Book Available  ");

        }else
            System.out.println(" No Book Available  ");

        promptEnterKey();
    }
    /*this method for borrow or return books for library
        1.role: set role(borrow or return) for this method
        2.condition: make sure status is Available(borrow) or Unavailable(return)
        3.setStatus : Change status from Available to Unavailable (borrow) or Unavailable to Available (return)

     */
    public  void processBook(String role,String condition, String setStatus){
        System.out.println("========= "+role.toUpperCase()+" BOOK INFO =========");
        int Id = validationNumber("=> Enter Book ID to "+role+" : ");
        if (Id>0 && Id<=id){
            for (int i=0;i<id;i++){
                if (Status[i]==condition){
                    if(Id==books[i].getId()){
                        System.out.println("Book ID        : "+ID[i]);
                        System.out.println("Book Title     : "+Title[i]);
                        System.out.println("Book Author    : "+Author[i]);
                        System.out.print("Published Year : "+py[i]);
                        System.out.println("  is "+role+"ed successfully...");
                        Status[i]=setStatus;
                    }
                }else
                    System.out.println("This book is "+setStatus+"!");
            }
        }
        else {
            System.out.println("Book ID : "+Id+" not Exist…");
        }
        promptEnterKey();
    }
    //this method for exit program
    public  void Exit(){
        System.out.println("-> Good bye!");
        System.exit(0);
    }
    // this method for create Menu
    public  void Menu(){
        String st = setup_library();// two values return from setup_library method into one variable st
        String[] str = st.split("\\|",-1);// split st into array  str
        while (true){
            System.out.println("========= "+str[0].toUpperCase()+","+str[1].toUpperCase()+" =========");
            System.out.println("1- Add Book");
            System.out.println("2- Show All Books");
            System.out.println("3- Show Available Books");
            System.out.println("4- Borrow Book");
            System.out.println("5- Return Book");
            System.out.println("6- Exit");
            System.out.println("-----------------------------------------");
            int choose = validationNumber("=> Choose option(1-6) : ");
            if (choose>0 && choose<=6){
                switch (choose){
                    case 1: addBook(id);
                        break;
                    case 2:  showBook("All");
                        break;
                    case 3: showBook("Available");
                        break;
                    case 4:  processBook("BORROW","Available","Unavailable");
                        break;
                    case 5:  processBook("RETURN","Unavailable","Available");
                        break;
                    case 6: Exit();
                        break;
                }

            }else{
                System.out.println("Please input menu from 1-6!");
                promptEnterKey();
            }
        }

    }
    // Main program
    public static void main(String[] args) {
        libraryManagement obj = new libraryManagement();
        obj.Menu();
    }
}
